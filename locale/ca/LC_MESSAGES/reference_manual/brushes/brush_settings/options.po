# Translation of docs_krita_org_reference_manual___brushes___brush_settings___options.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-30 03:35+0100\n"
"PO-Revision-Date: 2019-04-18 17:00+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/en/Krita_3_0_1_Brush_engine_ratio.png"
msgstr ".. image:: images/en/Krita_3_0_1_Brush_engine_ratio.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:1
msgid "Krita's Brush Engine options overview."
msgstr "Resum de les opcions per al motor de pinzells del Krita."

#: ../../reference_manual/brushes/brush_settings/options.rst:18
msgid "Options"
msgstr "Opcions"

#: ../../reference_manual/brushes/brush_settings/options.rst:24
msgid "Airbrush"
msgstr "Aerògraf"

#: ../../reference_manual/brushes/brush_settings/options.rst:27
msgid ".. image:: images/en/Krita_2_9_brushengine_airbrush.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_airbrush.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:28
msgid ""
"If you hold the brush still, but are still pressing down, this will keep "
"adding color onto the canvas. The lower the rate, the quicker the color gets "
"added."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:34
msgid "Mirror"
msgstr "Mirall"

#: ../../reference_manual/brushes/brush_settings/options.rst:37
msgid ".. image:: images/en/Krita_Pixel_Brush_Settings_Mirror.png"
msgstr ".. image:: images/en/Krita_Pixel_Brush_Settings_Mirror.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:38
msgid "This allows you to mirror the Brush-tip with Sensors."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:40
msgid "Horizontal"
msgstr "Horitzontal"

#: ../../reference_manual/brushes/brush_settings/options.rst:41
msgid "Mirrors the mask horizontally."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:43
msgid "Vertical"
msgstr "Vertical"

#: ../../reference_manual/brushes/brush_settings/options.rst:43
msgid "Mirrors the mask vertically."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:46
msgid ".. image:: images/en/Krita_2_9_brushengine_mirror.jpg"
msgstr ".. image:: images/en/Krita_2_9_brushengine_mirror.jpg"

#: ../../reference_manual/brushes/brush_settings/options.rst:47
msgid ""
"Some examples of mirroring and using it in combination with :ref:"
"`option_rotation`."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:52
msgid "Rotation"
msgstr "Gir"

#: ../../reference_manual/brushes/brush_settings/options.rst:54
msgid "This allows you to affect Angle of your brush-tip with Sensors."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:57
msgid ".. image:: images/en/Krita_2_9_brushengine_rotation.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_rotation.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:59
msgid ".. image:: images/en/Krita_Pixel_Brush_Settings_Rotation.png"
msgstr ".. image:: images/en/Krita_Pixel_Brush_Settings_Rotation.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:60
msgid "In the above example, several applications of the parameter."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:62
msgid ""
"Drawing Angle -- A common one, usually used in combination with rake-type "
"brushes. Especially effect because it does not rely on tablet-specific "
"sensors. Sometimes, Tilt-Direction or Rotation is used to achieve a similar-"
"more tablet focused effect, where with Tilt the 0° is at 12 o'clock, Drawing "
"angle uses 3 o'clock as 0°."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:63
msgid ""
"Fuzzy -- Also very common, this gives a nice bit of randomness for texture."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:64
msgid ""
"Distance -- With careful editing of the Sensor curve, you can create nice "
"patterns."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:65
msgid "Fade -- This slowly fades the rotation from one into another."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:66
msgid ""
"Pressure -- An interesting one that can create an alternative looking line."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:71
msgid "Scatter"
msgstr "Dispersió"

#: ../../reference_manual/brushes/brush_settings/options.rst:73
msgid ""
"This parameter allows you to set the random placing of a brush-dab. You can "
"affect them with Sensors."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:75
msgid "X"
msgstr "X"

#: ../../reference_manual/brushes/brush_settings/options.rst:76
msgid "The scattering on the angle you are drawing from."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:78
msgid "Y"
msgstr "Y"

#: ../../reference_manual/brushes/brush_settings/options.rst:78
msgid ""
"The scattering, perpendicular to the drawing angle (has the most effect)"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:81
msgid ".. image:: images/en/Krita_2_9_brushengine_scatter.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_scatter.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:85
msgid "Sharpness"
msgstr "Agudesa"

#: ../../reference_manual/brushes/brush_settings/options.rst:88
msgid ".. image:: images/en/Krita_Pixel_Brush_Settings_Sharpness.png"
msgstr ".. image:: images/en/Krita_Pixel_Brush_Settings_Sharpness.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:89
msgid "Puts a threshold filter over the brush mask."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:94
msgid "Size"
msgstr "Mida"

#: ../../reference_manual/brushes/brush_settings/options.rst:97
msgid ".. image:: images/en/Krita_Pixel_Brush_Settings_Size.png"
msgstr ".. image:: images/en/Krita_Pixel_Brush_Settings_Size.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:98
msgid ""
"This parameter is not the diameter itself, but rather the curve for how it's "
"affected."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:100
msgid ""
"So, if you want to lock the diameter of the brush, lock the Brush-tip. "
"Locking the size parameter will only lock this curve. Allowing this curve to "
"be affected by the Sensors can be very useful to get the right kind of "
"brush. For example, if you have trouble drawing fine lines, try to use a "
"concave curve set to pressure. That way you'll have to press hard for thick "
"lines."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:103
msgid ".. image:: images/en/Krita_2_9_brushengine_size_01.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_size_01.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:104
msgid ""
"Also popular is setting the size to the sensor fuzzy or perspective, with "
"the later in combination with a :ref:`assistant_perspective`"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:107
msgid ".. image:: images/en/Krita_2_9_brushengine_size_02.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_size_02.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:111
msgid "Softness"
msgstr "Suavitat"

#: ../../reference_manual/brushes/brush_settings/options.rst:113
msgid "This allows you to affect Fade with Sensors."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:116
msgid ".. image:: images/en/Krita_2_9_brushengine_softness.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_softness.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:117
msgid ""
"Has a slight brush-decreasing effect, especially noticeable with soft-brush, "
"and is overall more noticeable on large brushes."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:122
msgid "Source"
msgstr "Origen"

#: ../../reference_manual/brushes/brush_settings/options.rst:124
msgid "Picks the source-color for the brush-dab."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:126
msgid "Plain Color"
msgstr "Color llis"

#: ../../reference_manual/brushes/brush_settings/options.rst:127
msgid "Current foreground color."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:128
#: ../../reference_manual/brushes/brush_settings/options.rst:160
msgid "Gradient"
msgstr "Degradat"

#: ../../reference_manual/brushes/brush_settings/options.rst:129
msgid "Picks active gradient"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:130
msgid "Uniform Random"
msgstr "Aleatorietat uniforme"

#: ../../reference_manual/brushes/brush_settings/options.rst:131
msgid "Gives a random color to each brush dab."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:132
msgid "Total Random"
msgstr "Aleatorietat total"

#: ../../reference_manual/brushes/brush_settings/options.rst:133
msgid "Random noise pattern is now painted."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:134
msgid "Pattern"
msgstr "Patró"

#: ../../reference_manual/brushes/brush_settings/options.rst:135
msgid "Uses active pattern, but alignment is different per stroke."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:137
msgid "Locked Pattern"
msgstr "Patró bloquejat"

#: ../../reference_manual/brushes/brush_settings/options.rst:137
msgid "Locks the pattern to the brushdab"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:142
msgid "Mix"
msgstr "Mescla"

#: ../../reference_manual/brushes/brush_settings/options.rst:144
msgid ""
"Allows you to affect the mix of the :ref:`option_source` color with Sensors. "
"It will work with Plain Color and Gradient as source. If Plain Color is "
"selected as source, it will mix between foreground and background colors "
"selected in color picker. If Gradient is selected, it chooses a point on the "
"gradient to use as painting color according to the sensors selected."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:147
msgid ".. image:: images/en/Krita_2_9_brushengine_mix_01.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_mix_01.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:149
msgid "Uses"
msgstr "Usos"

#: ../../reference_manual/brushes/brush_settings/options.rst:152
msgid ".. image:: images/en/Krita_2_9_brushengine_mix_02.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_mix_02.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:154
msgid ""
"The above example uses a :program:`Krita` painted flowmap in the 3D program :"
"program:`Blender`. A brush was set to :menuselection:`Source --> Gradient` "
"and :menuselection:`Mix --> Drawing angle`. The gradient in question "
"contained the 360° for normal map colors. Flow maps are used in several "
"Shaders, such as brushed metal, hair and certain river-shaders."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:155
msgid "Flow map"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:162
msgid ""
"Exactly the same as using :menuselection:`Source --> Gradient` with :"
"guilabel:`Mix`, but only available for the Color Smudge Brush."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:168
msgid "Spacing"
msgstr "Espaiat"

#: ../../reference_manual/brushes/brush_settings/options.rst:171
msgid ".. image:: images/en/Krita_Pixel_Brush_Settings_Spacing.png"
msgstr ".. image:: images/en/Krita_Pixel_Brush_Settings_Spacing.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:172
msgid "This allows you to affect :ref:`option_brush_tip` with :ref:`sensors`."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:175
msgid ".. image:: images/en/Krita_2_9_brushengine_spacing_02.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_spacing_02.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:177
msgid "Isotropic spacing"
msgstr "Espaiat isotròpic"

#: ../../reference_manual/brushes/brush_settings/options.rst:177
msgid ""
"Instead of the spacing being related to the ratio of the brush, it will be "
"on diameter only."
msgstr ""

#: ../../reference_manual/brushes/brush_settings/options.rst:180
msgid ".. image:: images/en/Krita_2_9_brushengine_spacing_01.png"
msgstr ".. image:: images/en/Krita_2_9_brushengine_spacing_01.png"

#: ../../reference_manual/brushes/brush_settings/options.rst:185
msgid "Ratio"
msgstr "Relació"

#: ../../reference_manual/brushes/brush_settings/options.rst:187
msgid ""
"Allows you to change the ratio of the brush and bind it to parameters. This "
"also works for predefined brushes."
msgstr ""
