# Lithuanian translations for Krita Manual package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-05 03:37+0200\n"
"PO-Revision-Date: 2019-04-05 03:37+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"

#: ../../<generated>:1
msgid ".ppm"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:1
msgid "The pbm, pgm and ppm file formats as exported by Krita."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:17
msgid "\\*.pbm, \\*.pgm, \\*.ppm"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:18
msgid ""
".pbm, .pgm, .ppm are a series of file-formats with a similar logic to them. "
"They are designed to save images in a way that the result can be read as an "
"ascii file, from back when email clients couldn't read images reliably."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:20
msgid ""
"They are very old file formats, and not used outside of very specialized "
"usecases, such as embedding images inside code."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:22
msgid ".pbm"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:23
msgid "one-bit and can only show strict black and white."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:24
msgid ".pgm"
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:25
msgid "can show 255 values of gray (8bit)."
msgstr ""

#: ../../general_concepts/file_formats/file_pbgpm.rst:27
msgid "can show 8bit rgb values."
msgstr ""
