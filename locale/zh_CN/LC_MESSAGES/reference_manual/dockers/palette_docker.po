msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-04 03:36+0200\n"
"PO-Revision-Date: 2019-04-09 18:49\n"
"Last-Translator: guoyunhe <i@guoyunhe.me>\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___dockers___palette_docker.pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../reference_manual/dockers/palette_docker.rst:1
msgid "Overview of the palette docker."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:17
msgid "Palette Docker"
msgstr "调色板"

#: ../../reference_manual/dockers/palette_docker.rst:19
msgid ""
"The palette docker displays various color swatches for quick use. Since 4.0, "
"it also supports editing palettes and organizing colors into groups."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:22
msgid ".. image:: images/en/Palette-docker.png"
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:23
msgid ""
"You can choose from various default palettes or you can add your own colors "
"to the palette."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:25
msgid ""
"To choose from the default palettes click on the icon in the bottom left "
"corner of the docker, it will show a list of pre-loaded color palettes. You "
"can click on one and to load it into the docker, or click on import "
"resources (folder icon) to load your own color palette. Creating a new "
"palette can be done by filling out the :guilabel:`name` input, pressing :"
"guilabel:`Save` and selecting your new palette from the list."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:28
msgid "Selecting colors is done by |mouseleft| on a swatch."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:29
msgid ""
"Pressing the delete icon will remove the selected swatch or group. When "
"removing a group, Krita will always ask whether you'd like to keep the "
"swatches. If so, they will be added to the default group above."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:33
msgid ""
"Double |mouseleft| a swatch will call up the edit window where you can "
"change the color, the name, the id and whether it's a spot color. On a group "
"this will allow you to set the group name."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:34
msgid ""
"|mouseleft| drag will allow you to drag and drop swatches and groups to "
"order them."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:35
msgid "Pressing the :guilabel:`+` icon will allow you to add a new swatch."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:36
msgid "Pressing the Folder icon will allow you to add a new group."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:38
msgid "The edit and new color dialogs ask for the following:"
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:40
msgid "Color"
msgstr "彩色"

#: ../../reference_manual/dockers/palette_docker.rst:41
msgid "The color of the swatch."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:42
msgid "Name"
msgstr "文件名"

#: ../../reference_manual/dockers/palette_docker.rst:43
msgid "The Name of the color in a human readable format."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:44
msgid "ID"
msgstr "ID"

#: ../../reference_manual/dockers/palette_docker.rst:45
msgid ""
"The ID is a number that can be used to index colors. Where Name can be "
"something like \"Pastel Peach\", ID will probably be something like \"RY75\"."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:47
msgid "Spot color"
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:47
msgid ""
"Currently not used for anything within Krita itself, but spot colors are a "
"toggle to keep track of colors that represent a real world paint that a "
"printer can match. Keeping track of such colors is useful in a printing "
"workflow, and it can also be used with python to recognize spot colors."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:49
msgid ""
"If you find the size of color swatches too small, you can increase the size "
"by hovering your mouse over the palette and scrolling while holding :kbd:"
"`Ctrl`."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:51
msgid ""
"Krita's native palette format is since 4.0 :ref:`file_kpl`. It also supports "
"importing..."
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:53
msgid "Gimp Palettes (.gpl)"
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:54
msgid "Microsoft RIFF palette (.riff)"
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:55
msgid "Photoshop Binary Palettes (.act)"
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:56
msgid "PaintShop Pro palettes (.psp)"
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:57
msgid "Photoshop Swatches (.aco)"
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:58
msgid "Scribus XML (.xml)"
msgstr ""

#: ../../reference_manual/dockers/palette_docker.rst:59
msgid "Swatchbooker (.sbz)."
msgstr ""
