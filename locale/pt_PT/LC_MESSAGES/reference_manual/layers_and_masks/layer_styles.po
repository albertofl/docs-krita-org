# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-14 03:27+0100\n"
"PO-Revision-Date: 2019-03-14 11:49+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita Fx program menuselection Photoshop guilabel\n"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:1
msgid "How to use layer styles in Krita."
msgstr "Como usar os estilos das camadas no Krita."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:17
msgid "Layer Styles"
msgstr "Estilos da Camada"

#: ../../reference_manual/layers_and_masks/layer_styles.rst:19
msgid ""
"Layer styles are effects that are added on top of your layer. They are "
"editable and can easily be toggled on and off. To add a layer style to a "
"layer go to :menuselection:`Layer --> Layer Style`. You can also right-click "
"a layer to access the layer styles."
msgstr ""
"Os estilos da camada são os efeitos que são aplicados sobre a sua camada. "
"Eles são editáveis e podem ser facilmente activados ou desactivados. Para "
"adicionar um estilo à camada, vá a :menuselection:`Camada --> Estilo da "
"Camada`. Também poderá carregar com o botão direito sobre uma camada para "
"aceder aos estilos da mesma."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:22
msgid ""
"When you have the layer styles window up, make sure that the :guilabel:"
"`Enable Effects` item is checked."
msgstr ""
"Quando tiver visível a janela de estilos da camada, certifique-se que a "
"opção :guilabel:`Activar os Efeitos` está assinalada."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:24
msgid ""
"There are a variety of effects and styles you can apply to a layer. When you "
"add a style, your layer docker will show an extra \"Fx\" icon. This allows "
"you to toggle the layer style effects on and off."
msgstr ""
"Existe uma grande variedade de efeitos e estilos que poderá aplicar a uma "
"camada. Quando adicionar um estilo, a sua área da camada irá mostrar um "
"ícone \"Fx\" extra. Isto permite-lhe activar ou desactivar os efeitos do "
"estilo da camada."

#: ../../reference_manual/layers_and_masks/layer_styles.rst:30
msgid ""
"This feature was added to increase support for :program:`Adobe Photoshop`. "
"The features that are included mirror what that application supports."
msgstr ""
"Esta funcionalidade foi adicionada para aumentar o suporte para o :program:"
"`Adobe Photoshop`. As funcionalidades que estão incluídas reflectem o que "
"essa aplicação suporta."
