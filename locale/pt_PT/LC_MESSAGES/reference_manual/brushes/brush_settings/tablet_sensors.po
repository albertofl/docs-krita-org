# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-19 03:36+0100\n"
"PO-Revision-Date: 2019-02-26 18:05+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Krita guilabel Demão\n"

#: ../../<generated>:1
msgid "Tangential Pressure"
msgstr "Pressão Tangencial"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:1
msgid "Tablet sensors in Krita."
msgstr "Sensores da tablete no Krita."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:16
msgid "Sensors"
msgstr "Sensores"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:18
msgid "Pressure"
msgstr "Pressão"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:19
msgid "Uses the pressure in and out values of your stylus."
msgstr "Usa os valores de entrada e saída de pressão do seu lápis."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:20
msgid "PressureIn"
msgstr "Pressão de Entrada"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:21
msgid ""
"Uses only pressure in values of your stylus. Previous pressure level in same "
"stroke is overwritten *only* by applying more pressure. Lessening the "
"pressure doesn't affect PressureIn."
msgstr ""
"Usa apenas os valores de entrada da pressão do seu lápis. O nível de pressão "
"anterior no mesmo traço é substituído *apenas* se aplicar mais pressão. Se "
"aliviar a pressão, não irá afectar a Pressão de Entrada."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:22
msgid "X-tilt"
msgstr "Desvio em X"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:23
#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:25
msgid "How much the brush is affected by stylus angle, if supported."
msgstr ""
"Quanto é que o pincel é afectado pelo ângulo do lápis, se for suportado."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:24
msgid "Y-tilt"
msgstr "Desvio em Y"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:26
msgid "Tilt-direction"
msgstr "Direcção do desvio"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:27
msgid ""
"How much the brush is affected by stylus direction. The pen point pointing "
"towards the user is 0°, and can vary from -180° to +180°."
msgstr ""
"Quanto é que o pincel é afectado pela direcção do lápis. A ponta da caneta a "
"apontar para o utilizador corresponde a 0°, e este valor poderá variar entre "
"-180° e +180°."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:28
msgid "Tilt-elevation"
msgstr "Elevação do desvio"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:29
msgid ""
"How much the brush is affected by stylus perpendicularity. 0° is the stylus "
"horizontal, 90° is the stylus vertical."
msgstr ""
"Quanto é que o pincel é afectado pela perpendicularidade do lápis. O valor "
"0° corresponde ao lápis na horizontal, enquanto 90° é ao lápis na vertical."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:30
msgid "Speed"
msgstr "Velocidade"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:31
msgid "How much the brush is affected by the speed at which you draw."
msgstr "Quanto é que o pincel é afectado pela velocidade com que desenha."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:32
msgid "Drawing Angle"
msgstr "Ângulo de Desenho"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:33
msgid ""
"How much the brush is affected by which direction you are drawing in. :"
"guilabel:`Lock` will lock the angle to the one you started the stroke with. :"
"guilabel:`Fan corners` will try to smoothly round the corners, with the "
"angle being the angles threshold it'll round. :guilabel:`Angle offset` will "
"add an extra offset to the current angle."
msgstr ""
"Quanto é que o pincel é afectado pela direcção para a qual está a desenhar. "
"O :guilabel:`Bloquear` irá bloquear o ângulo com o que iniciou o traço. O :"
"guilabel:`Arredondar os cantos` irá tentar suavizar os cantos, onde o ângulo "
"corresponderá ao limite dos ângulos que irá arrendar. O :guilabel:"
"`Deslocamento do ângulo` irá adicionar um valor extra ao ângulo actual."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:34
msgid "Rotation"
msgstr "Rotação"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:35
msgid ""
"How much a brush is affected by how the stylus is rotated, if supported by "
"the tablet."
msgstr ""
"Como é que um pincel é afectado pela forma como o lápis está rodado, caso "
"seja suportado pela tablete."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:36
msgid "Distance"
msgstr "Distância"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:37
msgid "How much the brush is affected over length in pixels."
msgstr "Quanto é que o pincel é afectado pelo comprimento em pixels."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:38
msgid "Time"
msgstr "Hora"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:39
msgid "How much a brush is affected over drawing time in seconds.."
msgstr "Quanto é que um pincel é afectado pelo tempo do desenho em segundos."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:40
msgid "Fuzzy (Dab)"
msgstr "Difusão (Demão)"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:41
msgid "Basically the random option."
msgstr "Basicamente é uma opção aleatória."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:42
msgid "Fuzzy Stroke"
msgstr "Traço Difuso"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:43
msgid ""
"A randomness value that is per stroke. Useful for getting color and size "
"variation in on speed-paint brushes."
msgstr ""
"Um valor de aleatoriedade por cada traço. Útil para obter variações na cor e "
"no tamanho em pincéis de pintura rápida."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:44
msgid "Fade"
msgstr "Desvanecimento"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:45
msgid ""
"How much the brush is affected over length, proportional to the brush tine."
msgstr ""
"Quanto é que o pincel é afectado pelo comprimento, sendo proporcional ao "
"tempo do pincel."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:46
msgid "Perspective"
msgstr "Perspectiva"

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:47
msgid "How much the brush is affected by the perspective assistant."
msgstr "Quanto é que o pincel é afectado pelo assistente de perspectiva."

#: ../../reference_manual/brushes/brush_settings/tablet_sensors.rst:49
msgid ""
"How much the brush is affected by the wheel on airbrush-simulating stylii."
msgstr ""
"Quanto é que o pincel é afectado pela roda nos lápis de simulação de 'spray'."
