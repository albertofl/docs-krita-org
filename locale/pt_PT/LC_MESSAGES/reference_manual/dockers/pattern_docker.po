# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-19 03:36+0100\n"
"PO-Revision-Date: 2019-02-26 11:20+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en icons image Kritamouseleft kbd Kritamouseright\n"
"X-POFile-SpellExtra: images mouseleft alt KritaPatternsDocker mouseright\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../reference_manual/dockers/pattern_docker.rst:1
msgid "Overview of the pattern docker."
msgstr "Introdução à área de padrões."

#: ../../reference_manual/dockers/pattern_docker.rst:16
msgid "Patterns Docker"
msgstr "Área de Padrões"

#: ../../reference_manual/dockers/pattern_docker.rst:19
msgid ".. image:: images/en/Krita_Patterns_Docker.png"
msgstr ".. image:: images/en/Krita_Patterns_Docker.png"

#: ../../reference_manual/dockers/pattern_docker.rst:20
msgid ""
"This docker allows you to select the global pattern. Using the open-file "
"button you can import patterns. Some common shortcuts are the following:"
msgstr ""
"Esta área permite-lhe seleccionar o padrão global. Se usar o botão para "
"abrir ficheiros, poderá importar padrões. Seguem-se alguns atalhos comuns:"

#: ../../reference_manual/dockers/pattern_docker.rst:22
msgid "|mouseright| a swatch will allow you to set tags."
msgstr "Se usar o |mouseright| sobre uma área, poderá definir algumas marcas."

#: ../../reference_manual/dockers/pattern_docker.rst:23
msgid "|mouseleft| a swatch will allow you to set it as global pattern."
msgstr "Use o |mouseleft| sobre uma área para o definir como padrão global."

#: ../../reference_manual/dockers/pattern_docker.rst:24
msgid ":kbd:`Ctrl` + Scroll you can resize the swatch sizes."
msgstr ""
"Com o :kbd:`Ctrl` + Deslocamento, poderá ajustar os tamanhos das áreas."
