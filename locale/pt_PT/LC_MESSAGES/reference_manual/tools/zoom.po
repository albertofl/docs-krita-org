# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-10 03:30+0100\n"
"PO-Revision-Date: 2019-03-10 16:40+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: mouseright icons Krita navigation image\n"
"X-POFile-SpellExtra: Kritamouseleft Kritamousemiddle kbd Kritamouseright\n"
"X-POFile-SpellExtra: toolzoom images mousemiddle alt mouseleft ref\n"
"X-POFile-SpellExtra: zoomtool guilabel\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: botão direito"

#: ../../<rst_epilog>:6
msgid ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: mousemiddle"
msgstr ""
".. image:: images/icons/Krita_mouse_middle.png\n"
"   :alt: botão do meio"

#: ../../<rst_epilog>:82
msgid ""
".. image:: images/icons/zoom_tool.svg\n"
"   :alt: toolzoom"
msgstr ""
".. image:: images/icons/zoom_tool.svg\n"
"   :alt: ferramenta de ampliação"

#: ../../reference_manual/tools/zoom.rst:1
msgid "Krita's zoom tool reference."
msgstr "A referência da ferramenta de ampliação do Krita."

#: ../../reference_manual/tools/zoom.rst:16
msgid "Zoom Tool"
msgstr "Ferramenta de Ampliação"

#: ../../reference_manual/tools/zoom.rst:18
msgid "|toolzoom|"
msgstr "|toolzoom|"

#: ../../reference_manual/tools/zoom.rst:20
msgid ""
"The zoom tool allows you to zoom your canvas in and out discretely. It can "
"be found at the bottom of the toolbox, and you just activate it by selecting "
"the tool, and doing |mouseleft| on the canvas will zoom in, while |"
"mouseright| will zoom out."
msgstr ""
"A ferramenta de ampliação permite-lhe ampliar ou reduzir a sua área de "
"desenho de forma discreta. Poderá ser encontrada no fundo da área de "
"ferramentas e podê-la-á seleccionar se usar o |mouseleft| sobre a área de "
"desenho para ampliar ou o |mouseright| para reduzir."

#: ../../reference_manual/tools/zoom.rst:22
msgid "You can reverse this behaviour in the :guilabel:`Tool Options`."
msgstr ""
"Poderá reverter este comportamento nas :guilabel:`Opções da Ferramenta`."

#: ../../reference_manual/tools/zoom.rst:24
msgid ""
"There's a number of hotkeys associated with this tool, which makes it easier "
"to access from the other tools:"
msgstr ""
"Existem algumas combinações de teclas associadas a esta ferramenta, o que "
"facilitar o acesso a partir das outras ferramentas:"

#: ../../reference_manual/tools/zoom.rst:26
msgid ""
":kbd:`Ctrl + Space` + |mouseleft| + drag on the canvas will zoom in or out "
"fluently."
msgstr ""
"O :kbd:`Ctrl + Espaço` + |mouseleft| + arrastamento na área de desenho "
"ampliá-la-á ou reduzi-la-á de forma fluída."

#: ../../reference_manual/tools/zoom.rst:27
msgid ""
":kbd:`Ctrl` + |mousemiddle| + drag on the canvas will zoom in or out "
"fluently."
msgstr ""
"O :kbd:`Ctrl` + |mousemiddle| + arrastamento na área de desenho ampliá-la-á "
"ou reduzi-la-á de forma fluída."

#: ../../reference_manual/tools/zoom.rst:28
msgid ""
":kbd:`Ctrl + Alt + Space` + |mouseleft| + drag on the canvas will zoom in or "
"out with discrete steps."
msgstr ""
"O :kbd:`Ctrl + Alt + Espaço` + |mouseleft| + arrastamento na área de desenho "
"ampliá-la-á ou reduzi-la-á em passos discretos."

#: ../../reference_manual/tools/zoom.rst:29
msgid ""
":kbd:`Ctrl + Alt +` |mousemiddle| + drag on the canvas will zoom in or out "
"with discrete steps."
msgstr ""
"O :kbd:`Ctrl + Alt +` + |mousemiddle| + arrastamento na área de desenho "
"ampliá-la-á ou reduzi-la-á em passos discretos."

#: ../../reference_manual/tools/zoom.rst:30
msgid ":kbd:`+` will zoom in with discrete steps."
msgstr "O :kbd:`+` irá ampliar com passos discretos."

#: ../../reference_manual/tools/zoom.rst:31
msgid ":kbd:`-` will zoom out with discrete steps."
msgstr "O :kbd:`-` irá reduzir com passos discretos."

#: ../../reference_manual/tools/zoom.rst:32
msgid ":kbd:`1` will set the zoom to 100%."
msgstr "O :kbd:`1` irá repor o nível de ampliação em 100%."

#: ../../reference_manual/tools/zoom.rst:33
msgid ""
":kbd:`2` will set the zoom so that the document fits fully into the canvas "
"area."
msgstr ""
"O :kbd:`2` irá configurar a ampliação de forma que o documento caiba por "
"completo na área de desenho."

#: ../../reference_manual/tools/zoom.rst:34
msgid ""
":kbd:`3` will set the zoom so that the document width fits fully into the "
"canvas area."
msgstr ""
"O :kbd:`3` irá configurar a ampliação de forma que a largura do documento "
"caiba por completo na área de desenho."

#: ../../reference_manual/tools/zoom.rst:36
msgid "For more information on such hotkeys, check  :ref:`navigation`."
msgstr ""
"Para mais informações sobre essas combinações de teclas, veja a :ref:"
"`navigation`."
