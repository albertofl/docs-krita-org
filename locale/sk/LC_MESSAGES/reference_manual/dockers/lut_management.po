# translation of docs_krita_org_reference_manual___dockers___lut_management.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___lut_management\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-20 03:39+0100\n"
"PO-Revision-Date: 2019-04-02 09:47+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Set white and black points"
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:1
msgid "Overview of the LUT management docker."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:16
msgid "LUT Management"
msgstr "Správa LUT"

#: ../../reference_manual/dockers/lut_management.rst:19
msgid ".. image:: images/en/LUT_Management_Docker.png"
msgstr ".. image:: images/en/LUT_Management_Docker.png"

#: ../../reference_manual/dockers/lut_management.rst:20
msgid ""
"The Look Up Table (LUT) Management docker controls the high dynamic range "
"(HDR) painting functionality."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:22
msgid "Use OpenColorIO"
msgstr "Použiť OpenColorIO"

#: ../../reference_manual/dockers/lut_management.rst:23
msgid ""
"Use Open Color IO instead of Krita's internal color management. Open Color "
"IO is a color management library. It is sometimes referred to as OCIO. This "
"is required as Krita uses OCIO for its HDR functionality."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:24
msgid "Color Engine"
msgstr "Farebný engine"

#: ../../reference_manual/dockers/lut_management.rst:25
msgid "Choose the engine."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:27
msgid "Use an OCIO configuration file from your computer."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:31
msgid "Configuration"
msgstr "Nastavenie"

#: ../../reference_manual/dockers/lut_management.rst:31
msgid ""
"Some system locals don't allow you to read the configuration files. This is "
"due to a bug in OCIO. If you are using Linux you can fix this. If you start "
"Krita from the terminal with the ``LC_ALL=C krita`` flag set, you should be "
"able to read the configuration files."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:33
msgid "Input Color Space"
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:34
msgid "What the color space of the image is. Usually sRGB or Linear."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:35
msgid "Display Device"
msgstr "Zobrazovacie zariadenie"

#: ../../reference_manual/dockers/lut_management.rst:36
msgid ""
"The type of device you are using to view the colors. Typically sRGB for "
"computer screens."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:37
msgid "View"
msgstr "Zobraziť"

#: ../../reference_manual/dockers/lut_management.rst:38
msgid "--"
msgstr "--"

#: ../../reference_manual/dockers/lut_management.rst:39
msgid "Components"
msgstr "Komponenty"

#: ../../reference_manual/dockers/lut_management.rst:40
msgid "Allows you to study a single channel of your image with LUT."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:42
msgid "Exposure"
msgstr "Expozícia"

#: ../../reference_manual/dockers/lut_management.rst:42
msgid ""
"Set the general exposure. On 0.0 at default. There's :kbd:`Y` to change this "
"on the fly on canvas."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:44
msgid "Gamma"
msgstr "Gama"

#: ../../reference_manual/dockers/lut_management.rst:45
msgid ""
"Allows you to set the gamma. This is 1.0 by default. You can set this to "
"change on the fly in canvas shortcuts."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:46
msgid "Lock color"
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:47
msgid ""
"Locks the color to make sure it doesn't shift when changing exposure. May "
"not be desired."
msgstr ""

#: ../../reference_manual/dockers/lut_management.rst:49
msgid ""
"This allows you to set the maximum and minimum brightness of the image, "
"which'll adjust the exposure and gamma automatically to this."
msgstr ""
