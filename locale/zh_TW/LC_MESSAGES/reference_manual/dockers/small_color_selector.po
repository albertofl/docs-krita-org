# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-24 03:22+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Chinese <kde-i18n-doc@kde.org>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../reference_manual/dockers/small_color_selector.rst:1
msgid "Overview of the small color selector docker."
msgstr ""

#: ../../reference_manual/dockers/small_color_selector.rst:16
msgid "Small Color Selector"
msgstr ""

#: ../../reference_manual/dockers/small_color_selector.rst:19
msgid ".. image:: images/en/Krita_Small_Color_Selector_Docker.png"
msgstr ""

#: ../../reference_manual/dockers/small_color_selector.rst:20
msgid ""
"This is Krita's most simple color selector. On the left there's a bar with "
"the hue, and on the right a square where you can pick the value and "
"saturation."
msgstr ""
