# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-17 03:39+0200\n"
"PO-Revision-Date: 2019-03-12 17:50+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:None
msgid ".. image:: images/en/Krita_ghostlady_2.png"
msgstr ".. image:: images/en/Krita_ghostlady_2.png"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:None
msgid ".. image:: images/en/Krita_ghostlady_3.png"
msgstr ".. image:: images/en/Krita_ghostlady_3.png"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:1
msgid "How to use filter masks in Krita."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:16
msgid "Filter Masks"
msgstr "Filtermasker"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:18
msgid ""
"Filter masks show an area of their layer with a filter (such as blur, "
"levels, brightness / contrast etc.). For example, if you select an area of a "
"paint layer and add a Filter Layer, you will be asked to choose a filter. If "
"you choose the blur filter, you will see the area you selected blurred."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:24
msgid ""
"With filter masks, we can for example make this ghost-lady more ethereal by "
"putting a clone layer underneath, and setting a lens-blur filter on it."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:30
msgid ""
"Set the blending mode of the clone layer to :guilabel:`Color Dodge` and she "
"becomes really spooky!"
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:32
msgid ""
"Unlike applying a filter to a section of a paint layer directly, filter "
"masks do not permanently alter the original image. This means you can tweak "
"the filter (or the area it applies to) at any time. Changes can always be "
"altered or removed."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:34
msgid ""
"Unlike filter layers, filter masks apply only to the area you have selected "
"(the mask)."
msgstr ""

#: ../../reference_manual/layers_and_masks/filter_masks.rst:36
msgid ""
"You can edit the settings for a filter mask at any time by double clicking "
"on it in the Layers docker. You can also change the selection that the "
"filter mask affects by selecting the filter mask in the Layers docker and "
"then using the paint tools in the main window. Painting white includes the "
"area, painting black excludes it, and all other colors are turned into a "
"shade of gray which applies proportionally."
msgstr ""
